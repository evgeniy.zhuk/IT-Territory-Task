import {combineReducers} from "redux"
import {routerReducer as routing} from 'react-router-redux';
import {firebaseStateReducer as firebase} from "react-redux-firebase"

import todo from "./components/todo-list/TodoReducer";

export default combineReducers({
	todo,
	firebase,
	routing
});
