import React from 'react';
import {render} from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import configureStore from "./store";
import configureHistory from "./history";

if (process.env.NODE_ENV !== "production") {
	const {whyDidYouUpdate} = require("why-did-you-update");
	whyDidYouUpdate(React);
}

const mountApp = document.getElementById('root');
const store = configureStore();
const history = configureHistory();

render(
	<App store={store} history={history}/>
	, mountApp
);


registerServiceWorker();
