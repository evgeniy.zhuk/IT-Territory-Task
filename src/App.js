import React from 'react';
import {Provider} from 'react-redux'
import {ConnectedRouter} from "react-router-redux";
import PropTypes from "prop-types";
import logo from './logo.svg';
import './App.css';
import Schedule from "./modules/Schedule/Schedule";
import routes from './routes';

const App = ({store, history}) => {
	return (
		<Provider store={store}>
			<ConnectedRouter history={history}>
				<Schedule routes={routes}/>
			</ConnectedRouter>
		</Provider>
	);
};

App.propTypes = {
	store: PropTypes.object.isRequired,
	history: PropTypes.object.isRequired,
};

export default App;
