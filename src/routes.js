import React from "react";
import {Switch, Route} from "react-router"
import Loadable from "react-loadable";

const LazyHome = Loadable({
	loader: () =>
		import(/* webpackChunkName: "home" */ "./pages/home/Home"),
	loading: () => <div>Loading component</div>
});

const LazyNotFound = Loadable({
	loader: () =>
		import(/* webpackChunkName: "noMatch" */ "./pages/noMatch/NoMatch"),
	loading: () => <div>Loading component</div>
});

export default (
	<Switch>
		<Route exact path="/" component={LazyHome}/>
		<Route path="*" component={LazyNotFound}/>
	</Switch>
)
