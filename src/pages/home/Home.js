import React, {Fragment} from "react";
import Header from "../../components/header/Header";
import Footer from "../../components/footer/Footer";
import {connect} from "react-redux";
import TodoList from "../../components/todo-list/TodoList";


const HomePage = (props) => {

	console.log(props)

	return (
		<Fragment>
			{/*<Header/>*/}
			<main>
				Main content
			</main>
			<TodoList/>
			<Footer/>
		</Fragment>
	)
};


export default HomePage;

/*export default connect(state => ({
	todos: state.firebase.todos
}))(HomePage);*/
