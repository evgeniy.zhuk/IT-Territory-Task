import React from "react";
import "./NoMatch.scss";
import Status from "./components/Status/Status";


const NoMatch = () => (
	<Status code={404}>
		<div className="container-no-match">
			<div className="boo-wrapper">
				<div className="boo">
					<div className="face"/>
				</div>
				<div className="shadow"/>
				<h1>Уууупс!</h1>
				<p>
					Похоже, мы не можем найти
					<br/>
					страницу, которую вы ищите.
				</p>
			</div>
		</div>
	</Status>
);

export default NoMatch;
