import {
	applyMiddleware,
	compose,
	createStore
} from "redux";
import thunk from "redux-thunk";
import {routerMiddleware as router} from 'react-router-redux'
import rootReducer from "./reducers";
import * as FBS from "firebase"
import {getFirebase, reactReduxFirebase} from 'react-redux-firebase';
import {firebaseConfig, reactReduxFirebaseConfig} from "./firebase.config"
import configureHistory from "./history"


const configureStore = (initialState = {}) => {
	const fireBase = FBS.initializeApp(firebaseConfig);
	const thunkMiddleware = thunk.withExtraArgument(getFirebase);
	const routerMiddleware = router(configureHistory());

	const enhancers = [
		applyMiddleware(thunkMiddleware, routerMiddleware),
		reactReduxFirebase(fireBase, reactReduxFirebaseConfig)
	];

	const store = createStore(rootReducer, initialState, compose(...enhancers));

	if (module.hot) {
		module.hot.accept('./reducers', () => {
			const nextReducer = require('./reducers').default;
			store.replaceReducer(nextReducer);
		});
	}

	return store;
};

export default configureStore;
