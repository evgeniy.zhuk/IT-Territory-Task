import React, {Fragment} from "react";
import PropTypes from "prop-types";


const Schedule = ({routes}) => (
	<Fragment>
		{routes}
	</Fragment>
);

Schedule.propTypes = {
	routes: PropTypes.object.isRequired
};


export default Schedule;
