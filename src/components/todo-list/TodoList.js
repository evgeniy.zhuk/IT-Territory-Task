import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import {addToStorage} from "./TodoActionCreators";





class TodoList extends PureComponent {


	addItem = () => {
		this.props.dispatch(addToStorage({
			id: 1,
			desc: 'Drink Coffee!'
		}))
	};

	render() {
		return (
			<div>

				<button onClick={this.addItem()}>Click me</button>
			</div>
		);
	}
}

TodoList.propTypes = {};

export default connect()(TodoList);
