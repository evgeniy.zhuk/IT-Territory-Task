import {ADD_TASK} from "./TodoActionConstants"

const initialState = {
	todoList: []
};


const TodoReducer = (state = initialState, action) => {
	switch (action.type) {
		case ADD_TASK:
			return {
				...state,
				todoList: state.todoList.concat(action.payload.taskItem)
			};

		default:
			return state;
	}
};


export default TodoReducer;
