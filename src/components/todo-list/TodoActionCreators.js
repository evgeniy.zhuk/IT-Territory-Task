import {ADD_TASK} from "./TodoActionConstants";

export const addTask = (taskItem) => {
	return {
		type: ADD_TASK,
		payload: {
			taskItem
		}
	}
};


export const addToStorage = (newItem) =>
	(dispatch, getState, getFirebase) => {

		const firebase = getFirebase();

		console.log(firebase);

		firebase
			.push('todos', newItem)
			.then(() => {
				dispatch(addTask(newItem))
			})
	};
