import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';

class Header extends PureComponent {

	state = {
		isLoaded: false,
		data: {}
	};

	async componentDidMount() {
		const data = await this.fetchData();
		this.setState({
			isLoaded: true,
			data: {...data}
		})
	}

	fetchData = () => {
		const uri = "https://jsonplaceholder.typicode.com/posts/1";
		return fetch(uri)
			.then(res => res.json())
			.catch(err => console.error(err))
	};

	render() {
		return (
			<header>
				Header
				{this.state.isLoaded ? JSON.stringify(this.state.data) : <span>Loading...</span>}
			</header>
		);
	}
}

Header.propTypes = {};

export default Header;
